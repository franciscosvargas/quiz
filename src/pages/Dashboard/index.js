import React, { Component } from 'react'

import {
	Container
} from './styles'

import { Topbar } from '../../components'

class Dashboard extends Component {

	render() {
		return (
			<Container>
				<Topbar />
			</Container>
		);
	}
}

export default Dashboard

