import styled from 'styled-components'

export const Container = styled.div`
	display: flex;
	height: 100%;
	flex-direction: column;
	padding: 0px 40px 0px 40px;
	width: calc(100% - 80px);
`
