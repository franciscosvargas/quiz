# Quiz (Test JoyJet)

Hi! This is my test for the Joyjet tech interview.


# Required features

-   Develop a  **Quiz** application using React/VueJs/Angular depending on the role you are applying for;
-   The user can register a quiz, questions and answers;
-   A quiz may have several questions;
-   A question may have several answers;
-   When choosing a quiz, the user will be redirected to a page where he will list all the questions of that quiz and the answers to each question;
-   When answering a question, you must tell whether the choice is correct or not;
-   Each question must have at least 4 answers, one of them being the correct answer;
-   Npm or yarn should be used to manage the dependencies;
-   Create a README with installation guidelines;
-   Choose a theme (Bootstrap, Foundation...);
-   Use third-party libraries;
-   Choose how you will save the data (local storage, database, json...);

# My application

### What should you  expect?

I developed an application in ReactJS combining with a backend created to serve this application. Global states, api requests and a responsive layout are some of the key parts of this test.

### How to install

1º - Clone this repository;
2º - Use npm or yarn to install the libraries;
3º - Use the classical 'yarn start' or 'npx start' to run the project locally on localhost:3000;

